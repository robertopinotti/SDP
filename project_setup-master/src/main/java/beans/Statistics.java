package beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Statistics {

    // SETTINGS

    @XmlElement(name="statistics")
    private ArrayList<Statistic> statisticsList;
    private static Statistics instance;
    private Statistics(){
        statisticsList = new ArrayList<Statistic>();
    }

    // SERVICES

    // singleton
    public synchronized static Statistics getInstance(){
        if (instance==null)
            instance = new Statistics();
        return instance;
    }

    // get
    public synchronized List<Statistic> getStatisticsList(){
        return new ArrayList<Statistic>(statisticsList);
    }

    // add
    public synchronized void addStatistic(Statistic s){
        //System.out.println("Adding Statistic " + s.getId() + ": "+ s);
        statisticsList.add(s);
    }

    // getLastStatistics
    public synchronized List<Statistic> getLastStatistics(int n){
        List<Statistic> statisticsCopy = getStatisticsList();  // shallow copy
        int size = statisticsCopy.size();
        return statisticsCopy.subList(size-n,size); // from - to
    }

    // getDeliveries
    public float getDeliveriesAverage(long t1, long t2){

        //System.out.println("Statistics.java "+t1+" "+t2); // debug

        List<Statistic> statisticsCopy = getStatisticsList();  // shallow copy
        int deliveries=0;

        for(Statistic s: statisticsCopy) {
            if (s.getTimestamp() >= t1 && s.getTimestamp() <= t2) {
                //System.out.println("Statistics.java "+s.getId()); // debug
                deliveries++;
            }
        }

        return deliveries;
    }

    // getKm
    public float getKmAverage(long t1, long t2){
        List<Statistic> statisticsCopy = getStatisticsList();  // shallow copy
        int km=0;
        for(Statistic s: statisticsCopy)
            if (s.getTimestamp() >= t1 && s.getTimestamp() <= t2)
                km+=s.getKm();
        return km / statisticsCopy.size();
    }

}
