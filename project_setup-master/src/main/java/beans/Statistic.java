package beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Statistic {

    // fields
    private float deliveries;
    private float km;
    private float pollution;
    private float battery;
    private long timestamp;

    // empty constructor
    public Statistic(){}; // NON CANCELLARE ALTRIMENTI LA CHIAMATA REST NON FUNZIONA

    // SERVER REST (master --> server rest)
    public Statistic(float deliveries, float km, float pollution, float battery, long timestamp) {
        this.deliveries = deliveries;
        this.km = km;
        this.pollution = pollution;
        this.battery = battery;
        this.timestamp = timestamp;
    }

    // SINGLE DRONE (drone --> master)
    public Statistic(float km, float pollution, long timestamp) {
        this.km = km;
        this.pollution = pollution;
        this.timestamp = timestamp;
    }

    public float getDeliveries() {
        return deliveries;
    }

    public float getKm() {
        return km;
    }

    public float getPollution() {
        return pollution;
    }

    public float getBattery() {
        return battery;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setDeliveries(float deliveries) {
        this.deliveries = deliveries;
    }

    public void setKm(float km) {
        this.km = km;
    }

    public void setPollution(float pollution) {
        this.pollution = pollution;
    }

    public void setBattery(float battery) {
        this.battery = battery;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Statistic{" +
                "deliveries=" + deliveries +
                ", km=" + km +
                ", pollution=" + pollution +
                ", battery=" + battery +
                ", timestamp=" + timestamp +
                '}';
    }
}
