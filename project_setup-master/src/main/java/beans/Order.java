package beans;

public class Order {

    // fields
    private int id;
    private Point ritiro;
    private Point consegna;

    public Order(){}

    public Order(int id, Point ritiro, Point consegna) {
        this.id = id;
        this.ritiro = ritiro;
        this.consegna = consegna;
        //System.out.println(id+" "+ritiro+" "+consegna);//debug
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Point getRitiro() {
        return ritiro;
    }
    public void setRitiro(Point ritiro) {
        this.ritiro = ritiro;
    }
    public Point getConsegna() {
        return consegna;
    }
    public void setConsegna(Point consegna) {
        this.consegna = consegna;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", ritiro=" + ritiro +
                ", consegna=" + consegna +
                '}';
    }

}
