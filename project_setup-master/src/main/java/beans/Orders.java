package beans;

import java.util.ArrayList;

public class Orders {

    // fields
    private ArrayList<Order> ordersList;
    private static Orders instance;
    private Orders(){
        ordersList = new ArrayList<Order>();
    }

    // singleton
    public synchronized static Orders getInstance(){
        if (instance ==null)
            instance = new Orders();
        return instance;
    }

    // get
    public synchronized Order take(){

        //System.out.println("take");

        Order o = null;

        while(ordersList.size()<1){
            try{
                System.out.println("🚚⏰ No orders available...Waiting!");
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } // while

        if(ordersList.size()>0){
            o = ordersList.get(0);
            ordersList.remove(0);
        }

        return o;
    }

    // add
    public synchronized void add(Order o){
        ordersList.add(o);
        this.notify();
    }

    // remove
    public synchronized void remove(int id){
        for (Order o: ordersList)
            if (o.getId() == id)
                ordersList.remove(o);
    }

    // get
    public synchronized ArrayList<Order> getOrdersList(){
        return new ArrayList<>(ordersList);
    }

}
