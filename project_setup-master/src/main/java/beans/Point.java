package beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Point {

    private int x;
    private int y;

    // empty costructor
    public Point(){
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
        //System.out.println("Point.java "+x);//debug
    }

    public double distanceTo(Point that) {
        int dx = this.x - that.x;
        int dy = this.y - that.y;
        return Math.sqrt(dx*dx + dy*dy);
    }

    public String toString() {
        return x + "-" + y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
