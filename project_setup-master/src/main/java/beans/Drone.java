package beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Drone implements Comparable<Drone>{

    // fields
    private int id;
    private String ip;
    private int port;
    private Point position = new Point(0,0);;
    private int battery;
    private boolean delivering;

    // empty constructor
    public Drone(){

    }

    // constructor
    public Drone(int id, String ip, int port){

        int min=0, max=9;
        int x = (int)(Math.random() * ((max - min) + 1)) + min;
        int y = (int)(Math.random() * ((max - min) + 1)) + min;

        this.id = id;
        this.ip = ip;
        this.port = port;
        this.position = new Point(x,y);
        this.battery = 100;
        this.delivering = false;
    }

    // constructor
    public Drone(int id, String ip, int port, Point position, int battery){
        this.id = id;
        this.ip = ip;
        this.port = port;
        this.position = position;
        this.battery = battery;
    }

    // getter
    public int getId() {
        return id;
    }
    public String getIp() {
        return ip;
    }
    public int getPort() {
        return port;
    }
    public Point getPosition() {
        return position;
    }
    public int getBattery() {
        return battery;
    }
    public boolean isDelivering() {
        return delivering;
    }

    // setter
    public void setId(int id) {
        this.id = id;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public void setPort(int port) {
        this.port = port;
    }
    public void setPosition(Point position) {
        this.position = position;
    }
    public void setBattery(int battery) {
        this.battery = battery;
    }
    public void setDelivering(boolean delivering) {
        this.delivering = delivering;
    }

    @Override
    public String toString() {
        return "Drone{" +
                "id=" + id +
                ", ip=" + ip +
                ", port=" + port +
                ", position=" + position +
                ", battery=" + battery +
                '}';
    }

    @Override
    public int compareTo(Drone o) {
        return this.id - o.id; // ascending order
    }
}
