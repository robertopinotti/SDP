package beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Drones {

    // SETTINGS

    @XmlElement(name="drones")
    private ArrayList<Drone> dronesList;
    private static Drones instance;
    private Drones(){
        dronesList = new ArrayList<Drone>();
    }

    // SERVICES

    // singleton
    public synchronized static Drones getInstance(){
        if (instance==null)
            instance = new Drones();
        return instance;
    }

    // get
    public synchronized List<Drone> getDronesList(){
        return new ArrayList<>(dronesList);
    }

    // checkId
    public int checkId(int id){
        List<Drone> dronesCopy = getDronesList(); // shallow copy
        for (Drone d: dronesCopy)
            if (d.getId() == id)
                return 0; // ko - already exit
        return 1; // ok - does not exist yet
    }

    // add
    public synchronized void add(Drone d){
        dronesList.add(d);
    }

    // remove
    public synchronized int remove(int id){
        for (Drone d: dronesList) {
            if (d.getId() == id) {
                dronesList.remove(d);
                return 1; // ok
            }
        }
        return 0; // ko
    }

}
