package dronazon;

import beans.Order;
import beans.Point;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class OrdersGenerator extends Thread{

    private MqttClient client;
    private String clientId;
    private String topic;
    private int qos;
    private String broker;

    public OrdersGenerator(String clientId, String topic, int qos, String broker) {
        this.clientId = clientId;
        this.topic = topic;
        this.qos = qos;
        this.broker = broker;
    }

    @Override
    public void run() {

        System.out.println("\nThread OrdersGenerator started...");

        Order order;
        Point ritiro;
        Point consegna;

        try {
            client = new MqttClient(broker, clientId);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);

            // Connect the client
            System.out.println(clientId + " Connecting Broker " + broker);
            client.connect(connOpts); // bloccante
            System.out.println(clientId + " Connected");
        } catch (MqttException e) {
            e.printStackTrace();
        }

        /* INFINITE LOOP w/ 5" Sleep */
        for (int i=1;i>0;i++) {

            try {

                System.out.println("\n" + clientId + " Order #" + i);

                int id = i;
                int min = 0, max = 9;
                ritiro = new Point((int) (Math.random() * ((max - min) + 1)) + min, (int) (Math.random() * ((max - min) + 1)) + min);
                consegna = new Point((int) (Math.random() * ((max - min) + 1)) + min, (int) (Math.random() * ((max - min) + 1)) + min);
                order = new Order(id, ritiro, consegna);
                String payload = order.toString();

                MqttMessage message = new MqttMessage(payload.getBytes()); // getBytes converte il msg in binario
                message.setQos(qos);
                System.out.println(clientId + " Publishing message: " + payload.toString());
                client.publish(topic, message); // bloccante
                System.out.println(clientId + " Message published");

                Thread.sleep(5000);

            } catch (MqttException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        /* LOOP */

        System.out.println("\nThread OrdersGenerator terminated!");

        if (client.isConnected()) {
            try {
                client.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Publisher " + clientId + " disconnected");

    }
}
