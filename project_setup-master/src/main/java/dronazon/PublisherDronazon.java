package dronazon;

import org.eclipse.paho.client.mqttv3.MqttClient;

/*TERMINALE
brew services start mosquitto
brew services stop mosquitto
*/

public class PublisherDronazon {

    public static void main(String[] args) {

        String broker = "tcp://localhost:1883";
        String clientId = MqttClient.generateClientId();
        String topic = "dronazon/smartcity/orders";
        int qos = 2;

        /* THREAD generatore ordini*/
        OrdersGenerator ordersGenerator = new OrdersGenerator(clientId,topic,qos,broker);
        ordersGenerator.start();
        /* THREAD */

    }

}
