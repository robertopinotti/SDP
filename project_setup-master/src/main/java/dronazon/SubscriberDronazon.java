package dronazon;

import beans.Order;
import beans.Orders;
import beans.Point;
import org.eclipse.paho.client.mqttv3.*;

import java.util.Scanner;

public class SubscriberDronazon extends Thread{

    private static MqttClient client;

    public SubscriberDronazon(){

    }

    public void run(){

        System.out.println("🚚 Starting Broker MQTT...");

        String broker = "tcp://localhost:1883";
        String clientId = MqttClient.generateClientId();
        String topic = "dronazon/smartcity/orders";
        int qos = 2;

        try {
            client = new MqttClient(broker, clientId);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            // eventuali parametri opzionali

            // Connect the client
            //System.out.println(clientId + " Connecting Broker " + broker);
            client.connect(connOpts); // bloccante
            //System.out.println(clientId + " Connected - Thread PID: " + Thread.currentThread().getId());

            // Callback
            // permette a un MQTT client di lavorare in modalità asincrona
            // obbligatorio per i subscriber, opzionale per i publisher
            // quando fare la Callback: dopo la connect / prima della subscribe
            client.setCallback(new MqttCallback() {

                // 1. messageArrived
                // ricezione di un msg su uno specifico topic sul quale eravamo sottoscritti
                public void messageArrived(String topic, MqttMessage message) {

                    // Called when a message arrives from the server that matches any subscription made by the client
                    //String time = new Timestamp(System.currentTimeMillis()).toString();
                    String receivedMessage = new String(message.getPayload()); // da binario a stringa

                    // receivedMessage = Order{id=129, ritiro=9-8, consegna=7-2}
                    int id = Integer.parseInt(receivedMessage.split(",")[0].split("=")[1]);
                    int x1 = Integer.parseInt(receivedMessage.split(",")[1].split("=")[1].split("-")[0]);
                    int y1 = Integer.parseInt(receivedMessage.split(",")[1].split("=")[1].split("-")[1]);
                    int x2 = Integer.parseInt(receivedMessage.split(",")[2].split("=")[1].split("-")[0]);
                    int y2 = Integer.parseInt(receivedMessage.split(",")[2].split("=")[1].split("-")[1].split("}")[0]);

                    Point rit = new Point(x1,y1);
                    Point con = new Point(x2,y2);
                    Order order = new Order(id, rit, con);
                    Orders.getInstance().add(order); // uso del singleton
                    //System.out.println(order);

                    //System.out.println("\n ***  Press a random key to exit *** \n");

                }

                // 2. connectionLost
                // informa il client di una disconnessione inaspettata con il broker
                public void connectionLost(Throwable cause) {
                    System.out.println(clientId + " Connection lost! cause:" + cause.getMessage()+ "-  Thread PID: " + Thread.currentThread().getId());
                }

                // 3. deliveryComplete
                // se il msg è stato consegnato, o non è stato consegnato, al broker
                public void deliveryComplete(IMqttDeliveryToken token) {
                    // Not used here
                }

            });

            //System.out.println(clientId + " Subscribing ... - Thread PID: " + Thread.currentThread().getId());
            client.subscribe(topic,qos);
            // posso anche usare i vettori: .subscribre(["topic/a","topic/b"],[qos_a,qos_b])
            System.out.println("🚚 Subscribed to topics : " + topic);

            while(true){
                Scanner scanner = new Scanner(System.in);
                String input = scanner.next();
                if (input.equals("test")){
                    System.out.println("test");
                    //client.disconnect();
                }
            }

        } catch (MqttException me ) {
            System.out.println("🔴 MqttException");
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        } // catch

    } // run

    public static void disconnectClient(){
        //System.out.println("disconnectClient()");
        try {
            client.disconnect();
        } catch (MqttException e) {
            System.out.println("disconnectClient - Errore: " + e);
        }
        System.out.println("✅ MQTT Client disconnected successfully!");
    }

}
