package services;

import beans.*;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("statistics")
public class StatisticsServices {

    // print - debug
    // http://localhost:1337/statistics/
    // @Path("test")
    @GET
    @Produces("text/plain")
    public String helloStatistics(){
        return "Hello Statistics!";
    }

    // get - debug
    // http://localhost:1337/statistics/get
    @Path("get")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response getStatisticsList(){
        return Response.ok(Statistics.getInstance()).build();
    }

    /* INSERIMENTO STATISTIC
        Descrizione: il master aggiunge una statistica alla lista del server
        Input:
            - deliveries
            - km
            - pollution
            - battery
            - timestamp
        Output:
            - Errore, se l'inserimento fallisce
            - Ok, se l'inserimento va a buon fine
     */
    // http://localhost:1337/statistics/add
    @Path("add")
    @POST
    @Consumes({"application/json", "application/xml"})
    @Produces({"application/json", "application/xml"})
    public Response addStatistic(Statistic s){

        System.out.println("s: " + s);
        Statistics.getInstance().addStatistic(
                new Statistic(s.getDeliveries(),s.getKm(),s.getPollution(),s.getBattery(),s.getTimestamp())
        );
        return Response.ok("{\"message\": \"Statistic added correctly.\"}").build();

    }

    /* ULTIME STATS
        Descrizione: ritorna le ultime n statistiche dalla lista della statistiche
        Input:
            - n
        Output:
            - Ultime n statistiche
     */
    // http://localhost:1337/statistics/get/2
    @Path("get/{n}")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response getLastStatistics(@PathParam("n") int n){
        // se n è maggiore della lunghezza della lista --> ritorno errore
        if (Statistics.getInstance().getStatisticsList().size() == 0)
            return Response.status(Response.Status.NO_CONTENT).entity("{\"message\": \"There are no statistics.\"}").build();
        else if (Statistics.getInstance().getStatisticsList().size() < n)
            return Response.status(Response.Status.NO_CONTENT).entity("{\"message\": \"The size of the list is lower than n.\"}").build();
        else
            return Response.ok(Statistics.getInstance().getLastStatistics(n)).build();
    }

    /* MEDIA CONSEGNE tra due T
        Descrizione: ritorna la media delle consegne (tot consegne / numero di statistiche) tra due T
        Input:
            - T1
            - T2
        Output:
            - media delle consegne (float)
     */
    // http://localhost:1337/statistics/get/deliveries
    @Path("/get/deliveries/{t}")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response getDeliveriesAverage(@PathParam("t") String t){

        if(Statistics.getInstance().getStatisticsList().size() == 0){
            return Response.status(Response.Status.NO_CONTENT).entity("{\"message\": \"No statistics in the list.\"}").build();
        } else {
            //System.out.println("StatisticsServices " + t); // debug
            long t1 = Long.parseLong(t.split("-")[0]);
            long t2 = Long.parseLong(t.split("-")[1]);
            System.out.println("StatisticsServices " + t1); // debug
            System.out.println("StatisticsServices " + t2); // debug
            return Response.ok(Statistics.getInstance().getDeliveriesAverage(t1, t2)).build();
        }
    }

    /* MEDIA KM
        Descrizione: ritorna la media dei km percorsi (tot km / numero di statistiche)
        Input:
            -
        Output:
            - media dei km (float)
     */
    // http://localhost:1337/statistics/get/km
    @Path("/get/km/{t}")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response getKmAverage(@PathParam("t") String t){
        if(Statistics.getInstance().getStatisticsList().size() == 0){
            return Response.status(Response.Status.NO_CONTENT).entity("{\"message\": \"No statistics in the list.\"}").build();
        } else {
            long t1 = Long.parseLong(t.split("-")[0]);
            long t2 = Long.parseLong(t.split("-")[1]);
            return Response.ok(Statistics.getInstance().getKmAverage(t1,t2)).build();
        }
    }

}
