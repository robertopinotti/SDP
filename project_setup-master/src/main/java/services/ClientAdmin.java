package services;

import beans.Drone;
import beans.Drones;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.Scanner;

public class ClientAdmin {

    public static void main(String args[]) throws ParseException, IOException {

        int n = 0;
        Scanner in = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        do {

            System.out.println("\n******************************");
            System.out.println("Benvenuto nel client Admin.");
            System.out.println("Scegli una voce dal menu:");
            System.out.println("1. Elenco dei droni");
            System.out.println("2. Ultime n statistiche");
            System.out.println("3. Media numero consegne tra T1 e T2");
            System.out.println("4. Media km percorsi tra T1 e T2\n");

            n = in.nextInt();
            System.out.println("Hai selezionato: " + n); // testing

            if(n==1){

                Client client = Client.create();
                String url = "http://localhost:1337/drones/get";
                WebResource webResource = client.resource(url);
                ClientResponse clientResponse = webResource.accept("application/json").get(ClientResponse.class);
                //System.out.println("clientResponse: " + clientResponse.toString());
                Gson gson = new Gson();
                Drones drones = gson.fromJson(clientResponse.getEntity(String.class), Drones.class);
                //System.out.println("Drones List: " + drones.getDronesList());
                for (Drone d : drones.getDronesList()){
                    System.out.println(d);
                }

            } // if 1

            else if (n==2){

                do {
                    System.out.println("Quante statistiche vuoi?");
                    n = in.nextInt();
                } while(n<=0);

                try {

                    URL url = new URL("http://localhost:1337/statistics/get/"+n);
                    URLConnection urlConnection = url.openConnection();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line;
                    while ((line = bufferedReader.readLine()) != null)
                            System.out.println("Last "+n+" statistics: " + line);
                    bufferedReader.close();

                } catch (MalformedURLException e) {
                    //e.printStackTrace();
                    String msg = e.getMessage();
                    System.out.println("Error (MalformedURLException): "+msg);
                } catch (IOException e) {
                    //e.printStackTrace();
                    String msg = e.getMessage();
                    System.out.println("Error (IOException): "+msg);
                }

            } // if 2

            else if (n==3){

                System.out.println("Inserisci la prima data (mm/dd/yyyy hh:mm:ss):"); // 05/13/2021 10:05:06
                String data1 = br.readLine();
                // System.out.println(data1);
                System.out.println("Inserisci la seconda data (mm/dd/yyyy hh:mm:ss):");
                String data2 = br.readLine();
                // System.out.println(data2);

                long epoch1 = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
                        .parse(data1).getTime() / 1000; // Timestamp in seconds, remove '/1000' for milliseconds.
                long epoch2 = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
                        .parse(data2).getTime() / 1000; // Timestamp in seconds, remove '/1000' for milliseconds.

                // System.out.println(epoch1); // debug
                // System.out.println(epoch2);

                try {
                    URL url = new URL("http://localhost:1337/statistics/get/deliveries/"+epoch1+"-"+epoch2);
                    URLConnection urlConnection = url.openConnection();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line;
                    while ((line = bufferedReader.readLine()) != null)
                        System.out.println("Deliveries: " + line);
                    bufferedReader.close();
                } catch (MalformedURLException e) {
                    //e.printStackTrace();
                    String msg = e.getMessage();
                    System.out.println("Error (MalformedURLException): "+msg);
                } catch (IOException e) {
                    //e.printStackTrace();
                    String msg = e.getMessage();
                    System.out.println("Error (IOException): "+msg);
                }

            } // if 3

            else if (n==4){

                System.out.println("Inserisci la prima data (mm/dd/yyyy hh:mm:ss):"); // 05/13/2021 10:05:06
                String data1 = br.readLine();
                // System.out.println(data1);
                System.out.println("Inserisci la seconda data (mm/dd/yyyy hh:mm:ss):");
                String data2 = br.readLine();
                // System.out.println(data2);

                long epoch1 = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
                        .parse(data1).getTime() / 1000; // Timestamp in seconds, remove '/1000' for milliseconds.
                long epoch2 = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
                        .parse(data2).getTime() / 1000; // Timestamp in seconds, remove '/1000' for milliseconds.

                // System.out.println(epoch1); // debug
                // System.out.println(epoch2);

                try {
                    URL url = new URL("http://localhost:1337/statistics/get/km/"+epoch1+"-"+epoch2);
                    URLConnection urlConnection = url.openConnection();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line;
                    while ((line = bufferedReader.readLine()) != null)
                        System.out.println("Km: " + line);
                    bufferedReader.close();
                } catch (MalformedURLException e) {
                    //e.printStackTrace();
                    String msg = e.getMessage();
                    System.out.println("Error (MalformedURLException): "+msg);
                } catch (IOException e) {
                    //e.printStackTrace();
                    String msg = e.getMessage();
                    System.out.println("Error (IOException): "+msg);
                }

            } // if 4

            else {
                System.out.println("ATTENZIONE! Devi inserire un numero da 0 a 3.");
            }

            System.out.println("\nContinuare? 0 per uscire / altro numero per continuare\n");
            n = in.nextInt();
        } while (n != 0);

    }

}
