package services;

import beans.*;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("drones")
public class DronesServices {

    // print - debug
    // http://localhost:1337/drones/
    @GET
    @Produces("text/plain")
    public String helloDrones(){
        return "Hello Drones!";
    }

    // add n - debug
    // http://localhost:1337/drones/add/5
    @Path("add/{n}")
    @POST
    @Produces({"application/json", "application/xml"})
    public Response addDrones(@PathParam("n") int n){
        Drone drone;
        for (int i = 0; i < n; i++) {
            System.out.println("Adding Drone with Id="+i);
            drone = new Drone(i,"localhost",i);
            Drones.getInstance().add(drone);
        }
        return Response.ok(Drones.getInstance().getDronesList()).build();
    }

    /* LISTA DRONI
        Descrizione: il server ritorna la lista dei droni
        Input:
            -
        Output:
            - Lista dei droni
     */
    // http://localhost:1337/drones/get
    @Path("get")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response getDronesList(){
        Gson gson = new Gson();
        Drones drones = Drones.getInstance();
        return Response.status(Response.Status.OK).entity(gson.toJson(drones)).build();
        //return Response.ok(Drones.getInstance()).build();
    }

    /* INSERIMENTO DRONE
        Descrizione: il server aggiunge un drone al suo elenco di droni
        Input:
            - id, ip, port
        Output:
            - Errore, se l'inserimento fallisce
            - Lista dei droni, se l'inserimento va a buon fine
     */
    // http://localhost:1337/drones/add
    @Path("add")
    @POST
    @Consumes({"application/json", "application/xml"})
    @Produces({"application/json", "application/xml"})
    public Response addDrone(Drone d){

        int id = d.getId();
        Drone drone = new Drone(id,d.getIp(),d.getPort());

        if(Drones.getInstance().checkId(id) == 0) {
            return Response.status(Response.Status.CONFLICT).entity(
                    "{\"message\": \"Drone with ID=" + id + " already exist.\"}").build();
        } else {
            Drones.getInstance().add(drone);
            //System.out.println(Drones.getInstance().getDronesList());
            //return Response.ok( "{\"message\": \"Drone with ID=" + id + " added correctly.\"}").build();
            return Response.ok(Drones.getInstance().getDronesList()).build();
        }

    }

    /* RIMOZIONE DRONE
        Descrizione: il server deve semplicemente rimuove il nodo dall’elenco di nodi
        Input:
            - id
        Output:
            - Errore, se il drone non esiste
            - Ok, se il drone esiste
     */
    // http://localhost:1337/drones/remove/1
    @Path("remove/{id}")
    @DELETE
    @Produces("text/plain")
    public Response removeDrone(@PathParam("id") int id){

        if (Drones.getInstance().remove(id) == 1) // 1 = ok
            return Response.ok("✅ Node removed successfully!").build(); // production
            //return Response.ok(Drones.getInstance().getDronesList()).build(); // debug
        else
            return Response.status(Response.Status.NOT_FOUND).entity("Node with Id="+id+" doesn't exist.").build();
    }


}
