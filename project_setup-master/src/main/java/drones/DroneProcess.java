package drones;

import beans.*;
import beans.Point;
import com.example.drones.GrpcServiceGrpc;
import com.example.drones.GrpcServiceGrpc.*;
import com.example.drones.GrpcServiceOuterClass;
import com.example.drones.GrpcServiceOuterClass.*;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import dronazon.SubscriberDronazon;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import simulators.Measurement;
import simulators.Measurements;
import simulators.PM10Simulator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.List;

public class DroneProcess {

    private static Server server;

    public static void main(String[] args) {

        registrationMethod(); // 1. INIZIALIZZAZIONE + 2. REGISTRAZIONE
        welcomeServer(); // 3.2 WELCOME
        fromKeyboard(); // 4. USCITA DALLA RETE
        printStats(); // 6: STAMPA A SCHERMO

    } // main

    // 🤝 registration
    private static void registrationMethod() {

        //System.out.println("registrationMethod()");

        boolean success = false;
        int randomId;
        int min = 0, max = 1000;
        String ip = "localhost";
        int port = (int)Math.floor(Math.random()*(65535-49152+1)+49152);
        Client client = Client.create();
        WebResource webResource = client.resource("http://localhost:1337/drones/add");
        String input;
        ClientResponse response;
        JSONArray output = null;

        // contatto il server

        do {

            randomId = (int)Math.floor(Math.random()*(max-min+1)+min);

            try {

                input = "{\"id\":\"" + randomId + "\",\"ip\":\"" + ip + "\",\"port\":\"" + port + "\"}";
                //System.out.println("input: " + input); // debug

                response = webResource.type("application/json").post(ClientResponse.class, input);
                //System.out.println("response: " + response); // debug

                output = response.getEntity(JSONArray.class);
                //System.out.println("output: " + output); // debug
                
                success = true;

            } catch (Exception e) {
                
                System.out.println("registrationMethod 1 - Error (IOException): " + e.getMessage());
                
            }

        } while (!success);
        
        // aggiungo i droni dalla lista

        Drone drone; Point dronePosition;

        for (int i = 0; i < output.length(); i++) {

            try {

                JSONObject jsonObject = (JSONObject) output.get(i);
                int droneId;
                droneId = (int) jsonObject.get("id");
                String droneIp = (String) jsonObject.get("ip");
                int dronePort = (int) jsonObject.get("port");
                int x = Integer.parseInt(jsonObject.get("position").toString().substring(5, 6));
                int y = Integer.parseInt(jsonObject.get("position").toString().substring(11, 12));
                dronePosition = new Point(x, y);
                int droneBattery = (int) jsonObject.get("battery");

                drone = new Drone(droneId, droneIp, dronePort, dronePosition, droneBattery);

                if(droneId == randomId){ // sono io

                    DroneSingleton.getInstance().setCurrentDrone(
                            new Drone(randomId,ip,port,dronePosition, droneBattery)
                    );

                    System.out.println("👾 I am " +
                            DroneSingleton.getInstance().getCurrentDrone()
                    );

                } else { // non sono io
                    DroneSingleton.getInstance().addDroneList(drone);
                }

            } catch (JSONException e) {
                System.out.println("registrationMethod 2 - JSONException: " + e.getMessage());
            }

        } // for
        System.out.println("👾 dronesList: " + DroneSingleton.getInstance().getDronesList()); //debug

        // Quando l‘inserimento del drone nella smart-city va a buon fine,
        // esso dovrà avviare il proprio sensore per il rilevamento dell’inquinamento dell’aria.
        startSimulator(); // 3.1 AVVIO SENSORE INQUINAMENTO

    } // registrationMethod
    private static void welcomeServer() {

        //System.out.println("welcomeServer()");

        try {

            //System.out.println("👾 [GRPC SERVER] Launching chat service on port " + DroneSingleton.getInstance().getCurrentDrone().getPort());
            server = ServerBuilder
                    .forPort(DroneSingleton.getInstance().getCurrentDrone().getPort())
                    .addService(new GrpcServiceImpl())
                    .build();
            server.start();
            System.out.println("👾 GRPC SERVER started!");

            // METODO CLIENT
            if(!DroneSingleton.getInstance().getDronesList().isEmpty()) {
                welcomeClient();
            }else{
                System.out.println("👑 I am the MASTER"); // debug
                DroneSingleton.getInstance().setMasterDrone(
                        DroneSingleton.getInstance().getCurrentDrone()
                );

                masterThings();

            }

            //server.awaitTermination();

        } catch (IOException e) {
            System.out.println("welcomeServer -IOException error");
            e.printStackTrace();
        } catch (InterruptedException e){
            System.out.println("welcomeServer - InterruptedException error");
            e.printStackTrace();
        }

    } // welcomeServer
    private static void welcomeClient() throws InterruptedException{

        //System.out.println("welcomeClient()");
        //System.out.println("THREAD - GRPC CLIENT");

        ArrayList<Drone> dronesList = DroneSingleton.getInstance().getDronesList();
        System.out.println("droneList: " + dronesList);
        boolean nessunaRisposta = true;
        boolean nessunMaster = true;

        for(Drone drone : dronesList){

            System.out.println("🤝 Contacting Drone "+drone.getId() + "...");

            //opening a connection with the drone's server
            final ManagedChannel channel = ManagedChannelBuilder
                    .forTarget(drone.getIp() + ":" + drone.getPort())
                    .usePlaintext()
                    .build();

            //System.out.println("[GRPC CLIENT] channel: " + channel);
            //System.out.println("🤝 welcomeClient [GRPC Client] - Connected!");

            GrpcServiceBlockingStub stub = GrpcServiceGrpc.newBlockingStub(channel);

            GrpcServiceOuterClass.Point pos = GrpcServiceOuterClass.Point
                    .newBuilder()
                    .setX(DroneSingleton.getInstance().getCurrentDrone().getPosition().getX())
                    .setY(DroneSingleton.getInstance().getCurrentDrone().getPosition().getY())
                    .build();

            HelloRequest request = HelloRequest
                    .newBuilder()
                    .setId(DroneSingleton.getInstance().getCurrentDrone().getId())
                    .setPort(DroneSingleton.getInstance().getCurrentDrone().getPort())
                    .setIp(DroneSingleton.getInstance().getCurrentDrone().getIp())
                    .setPos(pos)
                    .setBat(DroneSingleton.getInstance().getCurrentDrone().getBattery())
                    .build();
            //System.out.println("[GRPC CLIENT] request: " + request);

            HelloResponse response;
            try {

                response = stub.greeting(request);
                //System.out.println("[GRPC CLIENT] response frome drone " + drone.getId() + ": " + response.getId());

                if(response.getId()!= -1) {
                    DroneSingleton.getInstance().setMasterDrone(
                            DroneSingleton.getInstance().getDroneFromId(response.getId())
                    );
                    nessunMaster = false;
                    ping(); // PING al MASTER
                } else {
                    System.out.println("👑 Master is " + DroneSingleton.getInstance().getMasterDrone());
                }

                nessunaRisposta = false;

            } catch (Exception e){

                //System.out.println("ERRORE: " + e.getMessage());
                System.out.println("🔴 welcomeClient - Non riesco a contattare il drone "+drone.getId());

                // rimuovo i droni dalle liste droni
                DroneSingleton.getInstance().removeDroneList(drone);

            }

            channel.shutdownNow();

        } // for

        if(nessunaRisposta){
            System.out.println("Nessuna risposta :/");
            // se nessun drone risponde, io divento il master
            DroneSingleton.getInstance().setMasterDrone(
                    DroneSingleton.getInstance().getCurrentDrone()
            );
            masterThings();
        }

        if(nessunMaster && !nessunaRisposta){
            System.out.println("Nessun master :/");

            // TODO caso limite: può essere che gli altri droni siano dentro una elezione

            // se nessun drone è master, allora faccio partire l'elezione
            election(1, DroneSingleton.getInstance().getCurrentDrone().getId()); // 1: election, 2: elected
        }

    } // welcomeClient

    // 📊 stats
    private static void startSimulator() {

        //System.out.println("startSimulator()");

        // produttore

        Measurements buffer = new Measurements();
        PM10Simulator pm10Simulator = new PM10Simulator(buffer);
        pm10Simulator.start();

        // consumatore

        ArrayList<Measurement> measurementList = new ArrayList<>(); // lista delle misurazioni
        //ArrayList<Measurement> averageList = new ArrayList<>(); // lista delle medie delle misurazioni
        new Thread(() -> { // lamba expression
            while(true) {

                measurementList.addAll( buffer.readAllAndClean() );
                double sum = 0; long timestamp = 0; int measurementId = 0;
                for(Measurement m : measurementList){
                    //System.out.println("m: " + m.getValue()); // debug
                    sum += m.getValue();
                    timestamp = m.getTimestamp();
                }
                DroneSingleton.getInstance().addAverageList(
                        new Measurement("pm10-"+measurementId++,"PM10",sum/8,timestamp)
                );
                measurementList.clear(); //sum = 0;
                //System.out.println("averageList: " + averageList); // debug

            }
        }).start();

    } // startSimulator
    private static void statsToServerThread() {

        // 10. MASTER

        //System.out.println("statsToServer()");
        System.out.println("📊 Starting sending stats to the SERVER...");

        new Thread(()->{
            while(true) {
                statsToServer();
                // ogni 10 secondi
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } // while
        }).start();

    } // statsToServerThread
    private static void statsToServer() {

        //System.out.println("statsToServer()");

        // 5.1 lista statistiche

        ArrayList<Statistic> statsList = DroneSingleton.getInstance().getGlobalStatsList();
        ArrayList<Drone> droneList = DroneSingleton.getInstance().getDronesList();
        droneList.add(DroneSingleton.getInstance().getCurrentDrone());

        //System.out.println("statsList: " + statsList); // debug // stampa anche il campo deliveries ma non è da guardare
        //System.out.println("droneList: " + droneList); // debug

        // la media delle consegne è somma delle consegne effettuate / numero di droni
        // il numero di consegne è il numero di stats nella lista statistiche globali
        // perché ad ogni consegna conclusa inserisco una statistica

        // operatore ternario
        // var = condizione ? vera : falsa

        float averageDeliveries = droneList.isEmpty() ? 0 : statsList.size() / droneList.size();
        float averageKm = 0; // media km percorsi - Statistic:km
        float averagePollution = 0; // media inquinamento - Statistic:pollution
        float averageBattery = 0; // media batteria - Statistic:battery
        Date date = new Date(); long timestamp = date.getTime(); // timestamp in ms

        if ( !statsList.isEmpty() ) { // se lista non vuota
            for (Statistic s : statsList) {
                averageKm += s.getKm();
                averagePollution += s.getPollution();
            }
            averageKm = droneList.isEmpty() ? 1 : averageKm / droneList.size();
            averagePollution = averagePollution / statsList.size();
        }

        if( ! droneList.isEmpty() ){
            for(Drone d : droneList){
                averageBattery += d.getBattery();
            }
            averageBattery = averageBattery / droneList.size();
        }

        // 5.2 invio REST

        Client client = Client.create();
        // Statistic : float deliveries, float km, float pollution, float battery, long timestamp
        Statistic s = new Statistic(averageDeliveries, averageKm, averagePollution, averageBattery, timestamp);
        System.out.println("📊 Stats to SERVER: " + s);

        WebResource webResource = client.resource("http://localhost:1337/statistics/add");
        String input = new Gson().toJson(s);
        ClientResponse clientResponse = null;
        try {
            //System.out.println(webResource.type("application/json").post(ClientResponse.class, input).toString());
            clientResponse = webResource.type("application/json").post(ClientResponse.class, input);
        } catch (ClientHandlerException e) {
            System.out.println("Server non disponibile");
        }
        //System.out.println("📊 SERVER Reply: " + clientResponse.toString());
        System.out.println("📊 SERVER Reply: " + clientResponse.getStatus());

        // 5.3 pulire la lista statistiche globali
        DroneSingleton.getInstance().clearGlobalStatsList();

    } // statsToServer()
    private static void printStats() {

        //System.out.println("printStats()");

        new Thread(()->{ // lambda expression

            while(true){
                System.out.println("📊 Drone "+DroneSingleton.getInstance().getCurrentDrone().getId()+" stats: " +
                        "Deliveries " + DroneSingleton.getInstance().getDeliveries() +
                        ", Km " + DroneSingleton.getInstance().getKm() +
                        ", Battery " + DroneSingleton.getInstance().getCurrentDrone().getBattery());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }).start();

    } // printStats

    // 🚚 delivery
    private static void startBroker() {

        //System.out.println("startBroker()");

        SubscriberDronazon thread = new SubscriberDronazon();
        thread.start();

        /*System.out.println("startBroker()");

        new Thread(()-> { // lambda expression

                // 2. Connessione al BROKER (MQTT)

                MqttClient client;
                String broker = "tcp://localhost:1883";
                String clientId = MqttClient.generateClientId();
                String topic = "dronazon/smartcity/orders";
                int qos = 2;

                try {
                    client = new MqttClient(broker, clientId);
                    MqttConnectOptions connOpts = new MqttConnectOptions();
                    connOpts.setCleanSession(true);
                    // eventuali parametri opzionali

                    // Connect the client
                    //System.out.println(clientId + " Connecting Broker " + broker);
                    client.connect(connOpts); // bloccante
                    //System.out.println(clientId + " Connected - Thread PID: " + Thread.currentThread().getId());

                    // Callback
                    // permette a un MQTT client di lavorare in modalità asincrona
                    // obbligatorio per i subscriber, opzionale per i publisher
                    // quando fare la Callback: dopo la connect / prima della subscribe
                    client.setCallback(new MqttCallback() {

                        // 1. messageArrived
                        // ricezione di un msg su uno specifico topic sul quale eravamo sottoscritti
                        public void messageArrived(String topic, MqttMessage message) {

                            // Called when a message arrives from the server that matches any subscription made by the client
                            //String time = new Timestamp(System.currentTimeMillis()).toString();
                            String receivedMessage = new String(message.getPayload()); // da binario a stringa

                            // receivedMessage = Order{id=129, ritiro=9-8, consegna=7-2}
                            int id = Integer.parseInt(receivedMessage.split(",")[0].split("=")[1]);
                            int x1 = Integer.parseInt(receivedMessage.split(",")[1].split("=")[1].split("-")[0]);
                            int y1 = Integer.parseInt(receivedMessage.split(",")[1].split("=")[1].split("-")[1]);
                            int x2 = Integer.parseInt(receivedMessage.split(",")[2].split("=")[1].split("-")[0]);
                            int y2 = Integer.parseInt(receivedMessage.split(",")[2].split("=")[1].split("-")[1].split("}")[0]);

                            Point rit = new Point(x1,y1);
                            Point con = new Point(x2,y2);
                            Order order = new Order(id, rit, con);
                            Orders.getInstance().add(order); // uso del singleton
                            //System.out.println(order);

                            //System.out.println("\n ***  Press a random key to exit *** \n");

                        }

                        // 2. connectionLost
                        // informa il client di una disconnessione inaspettata con il broker
                        public void connectionLost(Throwable cause) {
                            System.out.println(clientId + " Connection lost! cause:" + cause.getMessage()+ "-  Thread PID: " + Thread.currentThread().getId());
                        }

                        // 3. deliveryComplete
                        // se il msg è stato consegnato, o non è stato consegnato, al broker
                        public void deliveryComplete(IMqttDeliveryToken token) {
                            // Not used here
                        }

                    });

                    //System.out.println(clientId + " Subscribing ... - Thread PID: " + Thread.currentThread().getId());
                    client.subscribe(topic,qos);
                    // posso anche usare i vettori: .subscribre(["topic/a","topic/b"],[qos_a,qos_b])
                    System.out.println("🚚 Subscribed to topics : " + topic);

                    while(true){
                        Scanner scanner = new Scanner(System.in);
                        String input = scanner.next();
                        if (input.equals("quit")){
                            System.out.println("⏹ Disconnecting from MQTT Broker...");
                            client.disconnect();
                        }
                    }

                } catch (MqttException me ) {
                    System.out.println("reason " + me.getReasonCode());
                    System.out.println("msg " + me.getMessage());
                    System.out.println("loc " + me.getLocalizedMessage());
                    System.out.println("cause " + me.getCause());
                    System.out.println("excep " + me);
                    me.printStackTrace();
                } // catch

        }).start();*/

    } // startBroker
    private static void threadOrders() {

        new Thread(() -> { // lambda expression
            while (!DroneSingleton.getInstance().isExiting()) { // se sta uscendo non gestisco più ordini
                masterDelivery(Orders.getInstance().take());
            } // while
        }).start();

    } // threadOrders
    public static void masterDelivery(Order o) {

        //System.out.println("masterDelivery()");

        Drone closestDrone = new Drone();
        System.out.println("🚚 Processing " + o);

        boolean first = true;
        double distanceClosestDrone = 15; // max distance = 9 * radice2 = 12.8
        int x1 = o.getRitiro().getX(), y1 = o.getRitiro().getY(),x2,y2;

        /* OLD CODE
        ArrayList<Drone> allDronesList = DroneSingleton.getInstance().getDronesList(); // tutti i droni
        allDronesList.add(DroneSingleton.getInstance().getCurrentDrone());
        ArrayList<Drone> dronesList = new ArrayList<>(); // solo quelli che non stanno consegnando
        for(Drone d: allDronesList) {
            if (!d.isDelivering()) {
                dronesList.add(d);
            }
        }
         */
        ArrayList<Drone> dronesList = DroneSingleton.getInstance().getFreeDronesWaitNotify();
        //System.out.println("allDronesList: " + allDronesList); // debug
        //System.out.println("masterDelivery - dronesList: " + dronesList); // debug

        if(dronesList.size()<2){

            //System.out.println("<1");
            closestDrone = dronesList.get(0);

        } else {

            //System.out.println("else");
            //System.out.println("masterDelivery - dronesList: " + dronesList); // debug

            for (Drone d : dronesList) {

                //System.out.println("masterDelivery - for: " + d); // debug

                //if(DroneSingleton.getInstance().getDroneFromId(d.getId()).getBattery()<15){return;}

                x2 = d.getPosition().getX();
                y2 = d.getPosition().getY();
                double formula = Math.sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2);

                if (first) {
                    closestDrone = d;
                    distanceClosestDrone = formula;
                    first = false;
                } else {
                    // se distanza è minore della più corta, allora nuova più corta
                    if (formula < distanceClosestDrone) {
                        closestDrone = d;
                        distanceClosestDrone = formula;
                        // se ci sono più droni che hanno la stesso distanza
                    } else if (formula == distanceClosestDrone) {
                        //  controllare chi ha batteria maggiore
                        if (d.getBattery() > closestDrone.getBattery()) {
                            closestDrone = d;
                            distanceClosestDrone = formula;
                            //  Se batteria uguale
                        } else if (d.getBattery() == closestDrone.getBattery()) {
                            //  controllare ID maggiore.
                            if (d.getId() > closestDrone.getId()) {
                                closestDrone = d;
                                distanceClosestDrone = formula;
                                //System.out.println("closestDrone: " + closestDrone);
                            }
                        } // else if
                    } // else if
                } // else
            } // for d

        } // if

        System.out.println("🚚 The closest Drone is " + closestDrone);

        if(closestDrone==DroneSingleton.getInstance().getCurrentDrone()) {
            DroneSingleton.getInstance().getCurrentDrone().setDelivering(true);
            //System.out.println("delivering - " + DroneSingleton.getInstance().getCurrentDrone().isDelivering()); // debug
        }  else DroneSingleton.getInstance().setDelivering(closestDrone, true);

        // 3.3 consegna (GRPC)

        System.out.println("🚚 Contacting Drone "+closestDrone.getId() + "...");

        //opening a connection with server
        final ManagedChannel channel = ManagedChannelBuilder
                .forTarget(closestDrone.getIp() + ":" + closestDrone.getPort())
                .usePlaintext()
                .build();

        //System.out.println("[GRPC CLIENT] Connected!"); // debug

        GrpcServiceGrpc.GrpcServiceBlockingStub stub = GrpcServiceGrpc.newBlockingStub(channel);

        GrpcServiceOuterClass.Point con = GrpcServiceOuterClass.Point
                .newBuilder()
                .setX(o.getConsegna().getX())
                .setY(o.getConsegna().getY())
                .build();

        GrpcServiceOuterClass.Point rit = GrpcServiceOuterClass.Point
                .newBuilder()
                .setX(o.getRitiro().getX())
                .setY(o.getRitiro().getY())
                .build();

        GrpcServiceOuterClass.InizioConsegna request = GrpcServiceOuterClass.InizioConsegna
                .newBuilder()
                .setId(o.getId())
                .setCon(con)
                .setRit(rit)
                .build();

        //System.out.println("[GRPC CLIENT] request: " + request); // debug

        GrpcServiceOuterClass.FineConsegna response;

        try {

            response = stub.delivery(request);
            //System.out.println("[GRPC CLIENT] response: " + response); // debug

            // 4. ricezione stats dai droni (GRPC)

            // 4.1 ricezione delle stats

            Long timestamp = response.getTime();
            Point consegna = new Point(response.getPos().getX(), response.getPos().getY());
            Double km = response.getKm();
            List<Meas> measurements = response.getMeasList();
            int battery = response.getBat();

            // aggiorno la pos del drone che ha consegnato
            DroneSingleton.getInstance().setPosition(closestDrone, consegna);
            // aggiorno la batteria del drone che ha consegnato
            DroneSingleton.getInstance().setBattery(closestDrone, battery);

            // 4.2 salva le statistiche dentro la lista statistiche globali (sync)

            // il numero di consegne è il numero di stats nella lista statistiche globali
            // perché ad ogni consegna conclusa inserisco una statistica

            // media delle measurements
            float pollution = 0;
            for (Meas m : measurements) {
                pollution += m.getValue();
            }
            pollution = pollution / measurements.size();

            // inserire la statistica nella statistica globale
            DroneSingleton.getInstance().addGlobalStats(
                    new Statistic(km.floatValue(), pollution, timestamp)
            );

            // 4.3 quando arrivano le stats dal drone, significa che il drone ha completato la consegna

            if(closestDrone == DroneSingleton.getInstance().getCurrentDrone()){
                DroneSingleton.getInstance().getCurrentDrone().setDelivering(false);
                //System.out.println("delivering - " + DroneSingleton.getInstance().getCurrentDrone().isDelivering()); // debug
            } else DroneSingleton.getInstance().setDelivering(closestDrone, false);

        } catch (IllegalMonitorStateException e){

            //System.out.println("ERRORE: " + e);

        } catch (Exception e){

            //System.out.println("ERRORE: " + e);
            System.out.println("🚚🔴 The closest Drone " + closestDrone.getId() + " is unreachable :/");
            System.out.println("🚚 Trying contacting a new one...");

            // rimuovo il drone dalle liste droni
            DroneSingleton.getInstance().removeDroneList(closestDrone);

            // richiamo il metodo passando lo stesso ordine
            masterDelivery(o);

        }

        channel.shutdownNow();

    } // masterDelivery()

    // 🗳 election
    private static void ping() {

        //System.out.println("ping()");

        new Thread(() -> { // lambda expression
            boolean check = true;
            while (check) {

                Drone master = DroneSingleton.getInstance().getMasterDrone();

                if(master.getId() == DroneSingleton.getInstance().getCurrentDrone().getId() || DroneSingleton.getInstance().isParticipant() ){
                    // non devo più pingare se sono il master oppure sto facendo un'elezione
                    check = false;
                } else {

                    //System.out.println("[GRPC CLIENT] Contacting Drone "+master.getId() + "...");

                    //opening a connection with the drone's server
                    final ManagedChannel channel = ManagedChannelBuilder
                            .forTarget(master.getIp() + ":" + master.getPort())
                            .usePlaintext()
                            .build();

                    //System.out.println("[GRPC CLIENT] Connected!");

                    GrpcServiceBlockingStub stub = GrpcServiceGrpc.newBlockingStub(channel);

                    Ping request = Ping.newBuilder().setPing(
                            DroneSingleton.getInstance().getCurrentDrone().getId()
                    ).build();

                    Ping response;
                    try {

                        response = stub.ping(request);
                        //System.out.println("🏓 Ping - response: " + response.getPing());
                        System.out.println("🏓 Ping...👑 OK");

                        if(response.getPing()==2){
                            // devo presentarmi al master perché non mi ha nella sua lista droni
                            //System.out.println("response.getPing()==2");
                            welcomeClient();
                        }
                        if(response.getPing()==0){
                            // quel drone non è il master - faccio partire elezione
                            //System.out.println("response.getPing()==0");
                            election(1, DroneSingleton.getInstance().getCurrentDrone().getId()); // 1: election, 2: elected
                        }

                    } catch (Exception e) {

                        System.out.println("🏓 Ping...👑🔴 KO");
                        DroneSingleton.getInstance().removeDroneList(master);

                        // lancio elezione
                        election(1, DroneSingleton.getInstance().getCurrentDrone().getId()); // 1: election, 2: elected

                    }

                    channel.shutdownNow();

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                } // else

            } // while
        }).start();

    }
    public static void election(int type, int id){

        //System.out.println("election(type: "+type+", id: "+id+")");

        // 1. recupero lista droni ordinata per id e scelto il successivo

        ArrayList<Drone> droneList = DroneSingleton.getInstance().getDronesList();
        Drone currentDrone = DroneSingleton.getInstance().getCurrentDrone();

        //System.out.println("election - droneList: " + droneList);
        if(droneList.isEmpty()){

            //System.out.println("election - la lista di droni è vuota quindi io sono automaticamente eletto master");

            DroneSingleton.getInstance().setMasterDrone(
                    currentDrone
            );

            masterThings();

        } else {

            System.out.println("🗳 Election...");

            droneList.add(currentDrone);
            Collections.sort(droneList);

            // mia posizione nella lista di droni
            int myIndex = droneList.indexOf(currentDrone);

            // trovo mio successore
            Drone nextDrone;
            if (myIndex + 1 == droneList.size())
                nextDrone = droneList.get(0);
            else
                nextDrone = droneList.get(myIndex + 1);

            // 2. chiamata grpc a myIndex+1

            //System.out.println("[GRPC CLIENT] Contacting Drone " + nextDrone.getId() + "...");

            //opening a connection with the drone's server
            final ManagedChannel channel = ManagedChannelBuilder
                    .forTarget(nextDrone.getIp() + ":" + nextDrone.getPort())
                    .usePlaintext()
                    .build();

            //System.out.println("[GRPC CLIENT] Connected!");

            GrpcServiceBlockingStub stub = GrpcServiceGrpc.newBlockingStub(channel);

            Election request = Election
                    .newBuilder()
                    .setType(type) // 1: election, 2: elected
                    .setId(id)
                    .build();

            //System.out.println("[GRPC CLIENT] request: (" + request.getType() + ", " + request.getId() + ")");

            try {

                stub.election(request); // OLD Election response = stub.election(request);

            } catch (Exception e) {

                //System.out.println("ERRORE: " + e.getMessage());
                System.out.println("🗳🔴 election - Can't contact drone " + nextDrone.getId());

                // rimuovo il drone dalle liste droni
                DroneSingleton.getInstance().removeDroneList(nextDrone);

                // richiamo il metodo
                election(1, DroneSingleton.getInstance().getCurrentDrone().getId()); // 1: election, 2: elected

            } // catch

            channel.shutdownNow();

        } // else

    } // election
    public static void sendPosition() {

        //System.out.println("🎯 sendPosition");

        Drone currentDrone = DroneSingleton.getInstance().getCurrentDrone();
        Drone masterDrone = DroneSingleton.getInstance().getMasterDrone();

        System.out.println("🎯 position - Current Drone " + currentDrone.getId()
                + " is sending its position ("+ currentDrone.getPosition()
                + ") to Drone Master " + masterDrone.getId() + "...");

        //opening a connection with the drone's server
        final ManagedChannel channel = ManagedChannelBuilder
                .forTarget(masterDrone.getIp() + ":" + masterDrone.getPort())
                .usePlaintext()
                .build();

        GrpcServiceBlockingStub stub = GrpcServiceGrpc.newBlockingStub(channel);

        int x = currentDrone.getPosition().getX();
        int y = currentDrone.getPosition().getY();

        Position request = Position
                .newBuilder()
                .setId(currentDrone.getId())
                .setPoint(
                        GrpcServiceOuterClass.Point.newBuilder().setX(x).setY(y).build()
                )
                .build();

        //System.out.println("🎯 position - [GRPC CLIENT] request: " + request.getId() + " " + request.getPoint());

        try {

            stub.position(request); // OLD Position response = stub.position(request);

        } catch (Exception e) {

            //System.out.println("ERRORE: " + e.getMessage());
            System.out.println("🔴 position - Non riesco a contattare il drone master" + masterDrone.getId());

            // rimuovo il drone dalle liste droni
            DroneSingleton.getInstance().removeDroneList(masterDrone);

            // faccio partire una nuova elezione
            election(1, currentDrone.getId()); // 1: election, 2: elected

        } // catch

        channel.shutdownNow();

    }
    public static void masterThings() {

        //System.out.println("masterThings");
        System.out.println("👑 Faccio partire le cose da MASTER...");

        startBroker(); // 7. BROKER MQTT
        threadOrders(); // 8. CONSEGNE + 5. DELIVERY
        statsToServerThread(); // 10. STATS AL SERVER

    } // masterThings

    // ⚡️ recharge
    private static void recharge() {

        //System.out.println("recharge()");

        Drone currentDrone = DroneSingleton.getInstance().getCurrentDrone();
        ArrayList<Drone> droneList = DroneSingleton.getInstance().getDronesList();
        droneList.add(currentDrone);
        System.out.println("⚡️ Sending requests to " + droneList); // debug
        int count = 0;
        int size = droneList.size();

        String resource = "recharge";
        int id = currentDrone.getId();
        Date date = new Date(); long timestamp = date.getTime(); // timestamp in ms

        DroneSingleton.getInstance().setRecharging(1); // 0: non in uso, 1: voglio usarla, 2: la sto usando

        for(Drone d: droneList){

            System.out.println("⚡️ Contacting drone " + d.getId());

            //opening a connection with the drone's server
            final ManagedChannel channel = ManagedChannelBuilder
                    .forTarget(d.getIp() + ":" + d.getPort())
                    .usePlaintext()
                    .build();

            GrpcServiceBlockingStub stub = GrpcServiceGrpc.newBlockingStub(channel);

            SendRecharge request = SendRecharge
                    .newBuilder()
                    .setResource(resource)
                    .setId(id)
                    .setTimestamp(timestamp)
                    .build();

            try {

                ReplyRecharge response = stub.recharge(request);
                //System.out.println("response: " + response); // debug
                if(response.getReply().equals("OK")){
                    //System.out.println("count++");
                    count++;
                }

            } catch (Exception e){

                //System.out.println("ERRORE: " + e.getMessage());
                System.out.println("⚡️🔴 recharge - Can't contact drone " + d.getId());

                // rimuovo il drone dalle liste droni
                DroneSingleton.getInstance().removeDroneList(d);
                //droneList.remove(d);
                size--;

            } // catch

            channel.shutdownNow();

        } // for

        if(count == size){

            DroneSingleton.getInstance().setRecharging(2); // la sto usando

            // uso la risorsa
            try {
                System.out.println("⚡️ Charging...");
                Thread.sleep(10000);
                System.out.println("⚡️ Fully charged!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // invio al master nuova posizione (0,0) e nuova batteria (100%)

            if(DroneSingleton.getInstance().getCurrentDrone().getId()!=DroneSingleton.getInstance().getMasterDrone().getId()){
                // se non sono il master

                Drone master = DroneSingleton.getInstance().getMasterDrone();

                //opening a connection with the drone's server
                final ManagedChannel channel = ManagedChannelBuilder
                        .forTarget(master.getIp() + ":" + master.getPort())
                        .usePlaintext()
                        .build();

                GrpcServiceBlockingStub stub = GrpcServiceGrpc.newBlockingStub(channel);

                Position request = Position
                        .newBuilder()
                        .setId(currentDrone.getId())
                        .setPoint(
                                GrpcServiceOuterClass.Point.newBuilder().setX(-1).build()
                        )
                        .build();

                try {

                    stub.position(request);

                } catch (Exception e) {

                    //System.out.println("ERRORE: " + e.getMessage());
                    System.out.println("🔴⚡️ recharge - Non riesco a contattare il drone master" + master.getId());

                    // rimuovo il drone dalle liste droni
                    DroneSingleton.getInstance().removeDroneList(master);

                    // faccio partire una nuova elezione
                    election(1, currentDrone.getId()); // 1: election, 2: elected

                } // catch

                channel.shutdownNow();

            } else {
                // sono il master
                // aggiorno le mie info da master
                DroneSingleton.getInstance().getMasterDrone().setBattery(100);
                DroneSingleton.getInstance().getMasterDrone().setPosition( new Point(0,0) );
            }

            // in ogni caso, aggiorno le mie info da drone
            DroneSingleton.getInstance().getCurrentDrone().setBattery(100);
            DroneSingleton.getInstance().getCurrentDrone().setPosition( new Point(0,0) );

            // conclusione
            // se charging==0 allora posso consegnare

            DroneSingleton.getInstance().setRecharging(0); // non la sto usando

            synchronized (DroneSingleton.getInstance().getRechargeLock()){
                System.out.println("⚡️ recharge - NOTIFY ALL");
                DroneSingleton.getInstance().getRechargeLock().notifyAll(); // devo avvisare tutti
            }

        } // if

    } // recharge()

    // ⏹ exit
    private static void fromKeyboard() {

        //System.out.println("exitKeyboard()");

        new Thread(()->{ // lambda expression
            while(true) {
                Scanner scanner = new Scanner(System.in);
                //System.out.print("⏹ Send quit to stop the drone process...\n");
                String input = scanner.next();
                if (input.equals("quit")) {
                    try {
                        doExit();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (input.equals("recharge")) {
                    System.out.print("⚡️ recharge request\n");
                    if(DroneSingleton.getInstance().isExiting())
                        System.out.println("⚡️ I can't recharge, I'm quitting");
                    else
                        recharge();
                }
            } // while
        }).start();

    } // exitKeyboard
    public static void doExit() throws IOException {

        if(DroneSingleton.getInstance().isExiting()){return;}
        DroneSingleton.getInstance().setExiting(true);

        //System.out.println("doExit()");
        System.out.println("\n⏹ STOPPING everything...");

        // se un drone è in election, non può quittare
        System.out.println("isParticipant: " + DroneSingleton.getInstance().isParticipant()); // debug
        synchronized (DroneSingleton.getInstance().getElectionLock()){
            while(DroneSingleton.getInstance().isParticipant()){
                try {
                    System.out.println("⏹🗳 I can't quit, I'm inside an election...WAIT");
                    DroneSingleton.getInstance().getElectionLock().wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("⏹🗳 I'm not inside an election anymore.");
        }

        // se un drone sta consegnando, non può quittare
        //System.out.println("isDelivering: " + DroneSingleton.getInstance().isDelivering()); // debug
        synchronized (DroneSingleton.getInstance().getDeliveryLock()){
            while(DroneSingleton.getInstance().isDelivering()){
                try {
                    System.out.println("⏹🚚 I can't quit, I'm delivering...WAIT");
                    DroneSingleton.getInstance().getDeliveryLock().wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("⏹🚚️ I'm not delivering anymore.");
        }

        // 👑 SE DRONE È MASTER
        if(DroneSingleton.getInstance().getCurrentDrone().getId()==DroneSingleton.getInstance().getMasterDrone().getId()){
            //System.out.println("⏹ stopping - Questo Drone È il MASTER"); // DEBUG

            // 4.4 disconnettersi dal BROKER
            SubscriberDronazon.disconnectClient();

            // 4.5 assegnare le consegne pendenti agli altri droni
            ArrayList<Order> orders = Orders.getInstance().getOrdersList(); // lista degli ordini
            if(!orders.isEmpty()){
                System.out.println("Pending orders: " + orders);

                for(Order o: orders)
                    masterDelivery(o);

            } else {
                System.out.println("🚚 No pending orders.");
            }

            // 4.7 inviare al server le statistiche globali della smart city
            System.out.println("⏹ Sending last stats to the REST Server...");
            statsToServer();

        } // se drone è master

        // 4.2 + 4.6 chiudere le comunicazioni (channel grpc) con gli altri droni
        // le chiudo già ogni volta che le uso

        System.out.println("⏹ Shutting down the Grpc Server...");
        try {
            server.shutdownNow();
        } catch (Exception e){
            System.out.println("Errore nella chiusura del server: " + e);
        }

        // 4.3 comunicare l'uscita al server (REST)
        System.out.println("⏹ Contacting the REST Server...");
        int id = DroneSingleton.getInstance().getCurrentDrone().getId();
        try {
            URL url = new URL("http://localhost:1337/drones/remove/" + id);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("DELETE");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }

            bufferedReader.close();
            urlConnection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error: "+e.getMessage());
        }

        // 5. SUPER USCITA
        System.exit(0);

    } // doExit

} // class
