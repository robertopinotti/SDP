package drones;

import beans.Drone;
import beans.Point;
import com.example.drones.GrpcServiceGrpc.*;
import com.example.drones.GrpcServiceOuterClass;
import com.example.drones.GrpcServiceOuterClass.*;
import io.grpc.stub.StreamObserver;
import simulators.Measurement;

import javax.management.Query;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Queue;

public class GrpcServiceImpl extends GrpcServiceImplBase {

    @Override
    public void greeting(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {

        System.out.println("🤝 There is a new Drone in town --> Added " + request.getId());

        int x = request.getPos().getX();
        int y = request.getPos().getY();
        Point dronePosition = new Point(x, y);

        DroneSingleton.getInstance().addDroneList(
                new Drone(
                        request.getId(),
                        request.getIp(),
                        request.getPort(),
                        dronePosition,
                        request.getBat()
                )
        );

        int id = DroneSingleton.getInstance().getMasterDrone().getId()==DroneSingleton.getInstance().getCurrentDrone().getId() ? DroneSingleton.getInstance().getCurrentDrone().getId() : -1;

        HelloResponse response = HelloResponse
                .newBuilder()
                .setId(
                        id
                )
                .build();

        //System.out.println("🤝 greeting - [GRPC IMPL] response: " + response);

        responseObserver.onNext(response);
        responseObserver.onCompleted();

    } // greeting

    @Override
    public void delivery(InizioConsegna inizioConsegna, StreamObserver<FineConsegna> responseObserver) {

        //System.out.println("[GRPC Server] delivery"); // debug

        // se sto facendo recharge, non posso fare consegne
        if(DroneSingleton.getInstance().getRecharging()==2){

            synchronized (DroneSingleton.getInstance().getRechargeLock()){
                try {
                    System.out.println("🚚⚡️ I can't deliver, I'm recharging...WAIT");
                    DroneSingleton.getInstance().getRechargeLock().wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("🚚⚡️ Let's goooooooo");
            }
        } // if


        DroneSingleton.getInstance().setDelivering(true);

        // -10 quando il drone fa una consegna
        // il decremento di batteria l'ho messo qui perché nella documentazione c'è scritto che
        // un drone che sta facendo una consegna e inizia un'elezione deve essere considerato con la batteria già diminuita
        DroneSingleton.getInstance().getCurrentDrone().setBattery(
                DroneSingleton.getInstance().getCurrentDrone().getBattery() - 10
        );

        // A - RICEZIONE DELLA RICHIESTA

        //System.out.println("🚚 delivery - [GRPC IMPL]");

        int id = inizioConsegna.getId();
        int x1 = DroneSingleton.getInstance().getCurrentDrone().getPosition().getX();
        int y1 = DroneSingleton.getInstance().getCurrentDrone().getPosition().getY();
        int x2 = inizioConsegna.getCon().getX();
        int y2 = inizioConsegna.getCon().getY();
        int x3 = inizioConsegna.getRit().getX();
        int y3 = inizioConsegna.getRit().getY();
        Point ritiro = new Point(x2, y2);
        Point consegna = new Point(x3, y3);

        //System.out.println("delivery - [GRPC IMPL] Taking a nap...");
        System.out.println("🚚 Order " + id + " taken over by Drone " + DroneSingleton.getInstance().getCurrentDrone().getId());
        try {
            Thread.sleep(5000);
            System.out.println("🚚 Order " + id + " is being delivered...");
        } catch (Exception e) {
            System.out.println("Exception e: " + e.getMessage());
        }
        //System.out.println("delivery - [GRPC IMPL] Waken up!!");

        // B - INVIO DELLA RISPOSTA

        // 1. TIMESTAMP

        String timestamp = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
        long time = 0;
        try {
            time = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
                    .parse(timestamp).getTime() / 1000; // Timestamp in seconds, remove '/1000' for milliseconds.
        } catch (Exception e) {
            System.out.println("Exception e: " + e.getMessage());
        }

        // 2. POS CONSEGNA
        GrpcServiceOuterClass.Point posFinale = GrpcServiceOuterClass.Point
                .newBuilder()
                .setX(consegna.getX())
                .setY(consegna.getY())
                .build();

        // 3. KM PERCORSI

        double pos1pos2 = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
        double pos2pos3 = Math.sqrt(Math.pow(x3 - x2, 2) + Math.pow(y3 - y2, 2));
        double km = pos1pos2 + pos2pos3;

        // 4. LISTA MEDIE da ultima consegna
        ArrayList<Measurement> measurements = DroneSingleton.getInstance().getAverageList();

        // 5. BATTERIA RESIDUA
        int bat = DroneSingleton.getInstance().getCurrentDrone().getBattery();

        // CREZIONE CONSEGNA

        FineConsegna.Builder builder = FineConsegna.newBuilder()
                .setTime(time)
                .setPos(posFinale)
                .setKm(km)
                .setBat(bat);

        //https://stackoverflow.com/questions/32081493/how-to-set-google-protobuf-repeated-field-in-java
        for (Measurement m : measurements) {
            builder.addMeas(
                    GrpcServiceOuterClass.Meas.newBuilder().setValue(m.getValue()).setTime(m.getTimestamp())
            );
        }

        FineConsegna response = builder.build();

        //System.out.println("delivery - [GRPC IMPL] response: " + response);
        System.out.println("🚚 Order " + id + " delivered!");
        DroneSingleton.getInstance().clearAverageList(); // cancellare lista dopo ogni consegna

        // aggiorno mia posizione, numero di consegne e km
        DroneSingleton.getInstance().getCurrentDrone().setPosition(consegna);
        DroneSingleton.getInstance().addDeliveries();
        DroneSingleton.getInstance().addKm(km);

        responseObserver.onNext(response);
        responseObserver.onCompleted();

        //System.out.println("🔋 Battery Drone " + DroneSingleton.getInstance().getCurrentDrone().getId() +": " + DroneSingleton.getInstance().getCurrentDrone().getBattery() + "%"); // debug

        System.out.print("🔋 Battery Drone " + DroneSingleton.getInstance().getCurrentDrone().getId() + ": ");
        int b = DroneSingleton.getInstance().getCurrentDrone().getBattery();
        String s = "";
        for (int i = 0; i < 10; i++) {
            if (i < b / 10) s += "🟢";
            if (i >= b / 10) s += "🔴";
        }
        System.out.println(s + " " + b + "%");

        if (DroneSingleton.getInstance().getCurrentDrone().getBattery() < 15) {
            try {
                System.out.println("🔋 Battery below 15% --> Stopping the Drone");
                DroneSingleton.getInstance().setDelivering(false);
                DroneProcess.doExit();
            } catch (IOException e) {
                System.out.println("Exception e: " + e.getMessage());
            }
        } // if

        synchronized (DroneSingleton.getInstance().getDeliveryLock()) {
            //System.out.println("isDelivering: " + DroneSingleton.getInstance().isDelivering()); // debug
            DroneSingleton.getInstance().setDelivering(false);
            //System.out.println("isDelivering: " + DroneSingleton.getInstance().isDelivering()); // debug
            DroneSingleton.getInstance().getDeliveryLock().notify();

        } // sync

    } // delivery

    @Override
    public void election(Election request, StreamObserver<Election> responseObserver) {

        System.out.println("🗳 election - [GRPC IMPL]");

        Drone currentDrone = DroneSingleton.getInstance().getCurrentDrone();
        //System.out.println("currentDrone: "+ currentDrone.getId());
        Drone messageDrone = DroneSingleton.getInstance().getDroneFromId(request.getId());
        if (messageDrone.getId()==0) //*
            messageDrone = currentDrone;
        //System.out.println("messageDrone: "+ messageDrone.getId());

        //* un drone non tiene se stesso nella lista dei droni
        // quindi quando il token ha fatto il giro
        // la ricerca per id non dà risultato perché sta cercando se stesso

        if (request.getType() == 1) { // ELECTION
            //System.out.println("election - ELECTION");
            if(messageDrone.getId() == currentDrone.getId())
            {
                //System.out.println("election - I am MASTER");
                DroneSingleton.getInstance().setMasterDrone(currentDrone);
                DroneSingleton.getInstance().setParticipant(false);
                DroneProcess.election(2, currentDrone.getId());
            }
            else
            {
                if(messageDrone.getBattery() > currentDrone.getBattery()) // INOLTRO ID ARRIVATO
                {
                    //System.out.println("election - battery: >");
                    DroneSingleton.getInstance().setParticipant(true);
                    DroneProcess.election(1, messageDrone.getId());
                }
                else if(messageDrone.getBattery() < currentDrone.getBattery())
                {
                    //System.out.println("election - battery: <");
                    if(DroneSingleton.getInstance().isParticipant())
                    {
                        // IGNORO MESSAGGIO
                        //System.out.println("Ignoro il messaggio...");
                    }
                    else // INOLTRO MIO ID
                    {
                        DroneSingleton.getInstance().setParticipant(true);
                        DroneProcess.election(1, currentDrone.getId());
                    }
                }
                else // batteria uguale, guardo chi ha ID maggiore
                {
                    //System.out.println("election - battery: =");
                    if(messageDrone.getId() > currentDrone.getId()) // INOLTRO ID ARRIVATO
                    {
                        //System.out.println("election - id: >");
                        DroneSingleton.getInstance().setParticipant(true);
                        DroneProcess.election(1, messageDrone.getId());
                    }
                    else
                    {
                        //System.out.println("election - id: <");
                        if(DroneSingleton.getInstance().isParticipant())
                        {
                            // IGNORO MESSAGGIO
                            //System.out.println("Ignoro il messaggio...");
                        }
                        else // INOLTRO MIO ID
                        {
                            DroneSingleton.getInstance().setParticipant(true);
                            DroneProcess.election(1, currentDrone.getId());
                        }
                    }
                } // else batteria uguale
            } // else id diversi
        } // if ELECTION

        if (request.getType() == 2){ // ELECTED
            //System.out.println("election - ELECTED");
            if(messageDrone.getId() == currentDrone.getId()){
                // IGNORO MESSAGGIO perché ha fatto il giro dell'anello - ALGORITMO CONCLUSO
                System.out.println("🗳 election - ALGORITMO CONCLUSO, MASTER: "
                        + DroneSingleton.getInstance().getMasterDrone().getId());

                if(DroneSingleton.getInstance().getCurrentDrone().getId()
                        ==DroneSingleton.getInstance().getMasterDrone().getId())
                    DroneProcess.masterThings();

            }
            else
            {

                DroneSingleton.getInstance().setParticipant(false);
                DroneSingleton.getInstance().setMasterDrone(messageDrone);
                DroneProcess.election(2, messageDrone.getId());

                System.out.println("🗳 election - È stato eletto un nuovo master, ora gli invio la mia posizione...");
                DroneProcess.sendPosition();

            } // else id diversi

            synchronized (DroneSingleton.getInstance().getElectionLock()){
                System.out.println("🗳 electionLock: NOTIFY");
                DroneSingleton.getInstance().getElectionLock().notify();
            }

        } // if ELECTED

        // RESPONSE
        Election response = Election.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

    } // election

    @Override
    public void ping(Ping request, StreamObserver<Ping> responseObserver) {

        // RISPOSTA:
        // 0 se non sono il master
        // 1 se sono il master
        // 2 sono il master e non ho il drone salvato nella lista
        int risposta = 2;

        ArrayList<Drone> droneList = DroneSingleton.getInstance().getDronesList();
        for(Drone d: droneList)
            if(d.getId()==request.getPing())
                risposta = 1;

        if(DroneSingleton.getInstance().getMasterDrone().getId()!=DroneSingleton.getInstance().getCurrentDrone().getId())
            risposta = 0;

        System.out.println("🏓 ...pong!");

        Ping response = Ping.newBuilder().setPing(risposta).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

    } // ping

    @Override
    public void position(Position request, StreamObserver<Position> responseObserver) {

        //System.out.println("🎯 position - [GRPC IMPL]");

        System.out.println("🎯 position - [GRPC IMPL] Received info from Drone " + request.getId()
                + " with position (" + request.getPoint().getX() + ", " + request.getPoint().getY() + ")");

        int x = request.getPoint().getX();
        Drone d = DroneSingleton.getInstance().getDroneFromId(request.getId());

        if(x==-1){ // recharging
            DroneSingleton.getInstance().setPosition(d, new Point(0,0));
            DroneSingleton.getInstance().setBattery(d, 100);
        } else { // position
            int y = request.getPoint().getY();
            DroneSingleton.getInstance().setPosition(d, new Point(x,y));
        }

        System.out.println("🎯 position - [GRPC IMPL] New position of Drone " + request.getId()
                + " saved: " + DroneSingleton.getInstance().getDroneFromId(request.getId()).getPosition() );

        Position response = Position.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

    } // position

    @Override
    public void recharge(SendRecharge request, StreamObserver<ReplyRecharge> responseObserver) {

        System.out.println("⚡️ recharge - [GRPC IMPL] Received recharge request from Drone " + request.getId());

        String resource = request.getResource();
        int id = request.getId();
        long itsTime = request.getTimestamp();
        Date date = new Date(); long myTime = date.getTime(); // timestamp in ms

        if(DroneSingleton.getInstance().getRecharging()==0){ // non la uso
            ReplyRecharge response = ReplyRecharge.newBuilder().setReply("OK").build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
        if(DroneSingleton.getInstance().getRecharging()==1) { // se la voglio
            if(myTime<itsTime){ // caso 2

                synchronized (DroneSingleton.getInstance().getRechargeLock()){
                    try {
                        System.out.println("⚡️ recharge - WAIT 1");
                        DroneSingleton.getInstance().getRechargeLock().wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                // e poi rispondo
                ReplyRecharge response = ReplyRecharge.newBuilder().setReply("OK").build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();

            } else{ // caso 0
                ReplyRecharge response = ReplyRecharge.newBuilder().setReply("OK").build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();
            }
        }
        if(DroneSingleton.getInstance().getRecharging()==2){ // la sto usando

            synchronized (DroneSingleton.getInstance().getRechargeLock()){
                try {
                    System.out.println("⚡️ recharge - WAIT 2");
                    DroneSingleton.getInstance().getRechargeLock().wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            // e poi rispondo
            ReplyRecharge response = ReplyRecharge.newBuilder().setReply("OK").build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();

        }

    } // recharge

} // class