package drones;

import beans.*;
import simulators.Measurement;

import java.util.ArrayList;

public class DroneSingleton {

    // FIELDS
    private static DroneSingleton instance;
    private ArrayList<Drone> dronesList;
    private Drone currentDrone;
    private Drone masterDrone;
    private ArrayList<Measurement> averageList;
    private ArrayList<Statistic> globalStatsList;
    private volatile int deliveries;
    private volatile double km;

    private volatile boolean election = false; // election
    private volatile boolean delivering = false; // delivering
    private volatile boolean exiting = false; // exiting
    private boolean order = false;
    private volatile int recharging = 0; // 0: non in uso, 1: voglio usarla, 2: la sto usando

    private Object rechargeLock = new Object(); // lock durante recharging
    private Object deliveryLock = new Object(); // lock durante delivery
    private Object electionLock = new Object(); // lock durante delivery

    // CONSTRUCTOR
    private DroneSingleton(){
        currentDrone = new Drone();
        masterDrone = new Drone();
        dronesList = new ArrayList<>();
        averageList = new ArrayList<>();
        globalStatsList = new ArrayList<>();
    }

    // SINGLETON
    public synchronized static DroneSingleton getInstance(){
        if (instance==null)
            instance = new DroneSingleton();
        return instance;
    }

    // 👾 drones
    public ArrayList<Drone> getDronesList(){
        synchronized(dronesList) {
            return new ArrayList<>(dronesList);
        }
    }
    public void addDroneList(Drone d){
        synchronized (dronesList) {
            dronesList.add(d);
            //System.out.println("order: " + order);
            if(order) { // non chiamo notify se non sono ancora iniziati gli ordini
                dronesList.notify();
                //System.out.println("NOTIFY - un nuovo drone in da city");
            }
        }
    }
    public void removeDroneList(Drone d){
        synchronized(dronesList) {
            System.out.println("😢 Removing Drone " + d.getId());
            dronesList.remove(d);
        }
    }
    public void setPosition(Drone d, Point p){
        synchronized (dronesList) {
            for(Drone drone : dronesList){
                if(drone.getId()==d.getId()){
                    drone.setPosition(p);
                }
            }
        }
    }
    public void setBattery(Drone d, int b) {
        synchronized (dronesList) {
            for(Drone drone : dronesList){
                if(drone.getId()==d.getId()){
                    drone.setBattery(b);
                }
            }
        }
    }
    public Drone getDroneFromId(int id) {
        Drone drone = new Drone();
        ArrayList<Drone> dronesCopy = getDronesList(); // shallow copy
        dronesCopy.add(DroneSingleton.getInstance().getMasterDrone());
        //System.out.println("getDroneFromId - return dronesCopy: " + dronesCopy); // debug
        for (Drone d: dronesCopy) {
            //System.out.println("getDroneFromId - d: " + d); // debug
            if (d.getId() == id) {
                drone=d;
            }
        }
        //System.out.println("getDroneFromId - return drone: " + drone); // debug
        return drone;
    }

    // 👾 current drone
    public Drone getCurrentDrone(){
        synchronized (currentDrone) {
            return currentDrone;
        }
    }
    public void setCurrentDrone(Drone d) {
        synchronized (currentDrone) {
            currentDrone = d;
        }
    }

    // 👑 master
    public Drone getMasterDrone() {
        synchronized (masterDrone) {
            return masterDrone;
        }
    }
    public void setMasterDrone(Drone d) {
        synchronized (masterDrone) {
            System.out.println("👑 New Master: Drone " + d.getId());
            masterDrone = d;
        }
    }

    // 💨 average stats - list of measurements from the PM10 simulator
    public ArrayList<Measurement> getAverageList() {
        synchronized (averageList) {
            return averageList;
        }
    }
    public void addAverageList(Measurement m) {
        synchronized (averageList){
            averageList.add(m);
        }
    }
    public void clearAverageList() {
        synchronized (averageList) {
            averageList.clear();
        }
    }

    // 📊 global stats - list of statistics from drones
    public ArrayList<Statistic> getGlobalStatsList(){
        synchronized (globalStatsList) {
            return new ArrayList<>(globalStatsList);
        }
    }
    public void addGlobalStats(Statistic s){
        synchronized (globalStatsList) {
            globalStatsList.add(s);
        }
    }
    public void clearGlobalStatsList() {
        synchronized (globalStatsList) {
            globalStatsList.clear();
        }
    }

    // 🚚 deliveries
    public int getDeliveries() {
        return deliveries;
    }
    public void addDeliveries() {
        deliveries++;
    }
    public void setDelivering(Drone d, boolean del){
        //System.out.println("setDelivering()");
        synchronized(dronesList) {
            //System.out.println("setDelivering() - synchronized");
            for(Drone drone: dronesList){
                if(drone.getId()==d.getId()){
                    drone.setDelivering(del);
                    //System.out.println("isDelivering: " + drone.isDelivering());
                }
            }
        } // sync
        if(!del) {
            //System.out.println("NOTIFY - un drone ("+d.getId()+") si è liberato, del: " + del);
            dronesList.notify();
        }
    }
    public double getKm() {
        return km;
    }
    public void addKm(double km) {
        this.km += km;
    }
    public ArrayList<Drone> getFreeDronesWaitNotify(){

        //System.out.println("getFreeDronesWaitNotify");
        order = true;

        synchronized (dronesList) {

            while( getFreeDrones().isEmpty() ){
                try {
                    System.out.println("👾⏰ No drones available...Waiting!");
                    dronesList.wait();
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException - Errore nella wait: " + e);
                }
            } // while

            //System.out.println("EXIT THE WAIT - freeDrones: " + getFreeDrones());

            return getFreeDrones();
        } // sync
    }
    private ArrayList<Drone> getFreeDrones() {

        ArrayList<Drone> copyDrones = getDronesList();
        //System.out.println("getFreeDrones - copyDrones: " + copyDrones);//debug
        ArrayList<Drone> freeDrones = new ArrayList<>();

        copyDrones.add(currentDrone);
        for (Drone d : copyDrones)
            if (!d.isDelivering() && d.getBattery()>10) // se il drone non sta né consegnando né facendo una ricarica
                freeDrones.add(d);

        //System.out.println("getFreeDrones - freeDrones: " + freeDrones);

        return freeDrones;
    }
    public boolean isDelivering() {
        return delivering;
    }
    public void setDelivering(boolean delivering) {
        this.delivering = delivering;
    }
    public Object getDeliveryLock() {
        return deliveryLock;
    }

    // ⚡️ recharge
    public int getRecharging() {
        return recharging;
    }
    public void setRecharging(int recharging) {
        this.recharging = recharging;
    }
    public Object getRechargeLock() {
        return rechargeLock;
    }

    // 🗳 election
    public boolean isParticipant() {
        return election;
    }
    public void setParticipant(boolean election) {
        this.election = election;
    }
    public Object getElectionLock() {
        return electionLock;
    }

    // ⏹ exit
    public boolean isExiting() {
        return exiting;
    }
    public void setExiting(boolean exiting) {
        this.exiting = exiting;
    }

}
